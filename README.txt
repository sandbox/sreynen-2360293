The Component Themes module allows themes to have multiple base themes, treating base themes as smaller components intended to be used for more specific purposes in multiple full themes. To use this, simply reference theme components in your theme .info file, like so:

component themes[] = square_buttons
component themes[] = dates
component themes[] = slideshow

Component themes function as base themes, and register in the order they are listed, so each component theme will act as the base theme of the one before. And the first component theme will act as the base theme of your standard base theme.
